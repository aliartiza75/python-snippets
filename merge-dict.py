# Supported in python3.5
w = {"5":"e", "6":"f"}
x = {"1": "a", "2": "b"}
y = {"3": "c", "4": "d"}

z = {**x, **y, **w}

print(z)

