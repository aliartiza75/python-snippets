"""
Written by: Irtiza Ali
Date: 20/09/2018
Desription: It contains an example related to python reduce function. In this example we will calculate factorial using 
            reduce
"""
from functools import reduce

num = 4

nums = list(range(1, num + 1))
factorial = reduce(lambda x, y: x * y, nums)

print(factorial)