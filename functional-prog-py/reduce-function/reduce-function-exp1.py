"""
Written by: Irtiza Ali
Date: 20/09/2018
Desription: It contains an example related to python reduce function. In this example we will try to
            add the elements of the list
"""
from functools import reduce

nums = [1, 2, 3, 4]
res = reduce(lambda x, y: x + y, nums)

print(res)