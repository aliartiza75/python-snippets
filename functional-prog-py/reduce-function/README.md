# Reduce Function

reduce() function is from the functools package. The reduce() function takes in an iterable, and then reduces the iterable to a single value. Reduce is different from filter() and map(), because reduce() takes in a function that has two input values.

An interesting note to make is that I do not have to operate on the second value in the lambda expression.

##### Image describing the working of the Reduce function

![image](https://dq-content.s3.amazonaws.com/263/s5_reduce_function.svg)