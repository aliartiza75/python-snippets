"""
Written by: Irtiza Ali
Date: 15/09/2018
Desription: It contains an example related to python lambda expressions
            In this example I am sorting a tuple array on the basis of seconds element
"""

unsorted_tuples = [('b', 6), ('a', 10), ('d', 0), ('c', 4)]

# sorting the tuple using the seconds element
# sorted_tuples = sorted(unsorted_tuples , key=lambda x: x[1])


sorted_tuples = sorted(unsorted_tuples , key=lambda x: x[1])
print(sorted_tuples)

sorted_tuples2 = sorted(unsorted_tuples, reverse=True)
print(sorted_tuples2)
