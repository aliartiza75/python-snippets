"""
Written by: Irtiza Ali
Date: 15/09/2018
Desription: It contains an example related to python lambda expressions
"""

# old example
def addition(a, b):
    return a + b

print(addition(1, 2))


# addition method using lambda expression
lambda_addition = lambda a, b: a + b
print(lambda_addition(1 ,2))