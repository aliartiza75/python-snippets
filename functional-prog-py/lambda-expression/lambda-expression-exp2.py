"""
Written by: Irtiza Ali
Date: 15/09/2018
Desription: It contains an example related to python lambda expressions
            In this example I am using condition statement in the lambda expression
"""

which_is_greater = lambda a,b: a if a > b else b


result = which_is_greater(1,2)
print(result)