# It will have functional programming code examples in python

## Concepts

* `Pure Function` is a functions that doesn't maintain any state, it just takes input and produce output
* `lambda Expression` is an expression that takes in a comma seperated sequences of inputs (like def). Then, immediately following the colon, it returns the expression without using an explicit return statement.





## Resources

* https://www.dataquest.io/blog/introduction-functional-programming-python/
* https://medium.com/@happymishra66/lambda-map-and-filter-in-python-4935f248593
