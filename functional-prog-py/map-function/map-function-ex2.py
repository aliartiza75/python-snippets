"""
Written by: Irtiza Ali
Date: 18/09/2018
Desription: It contains an example related to python map functions
"""
import math

values = [(1, 1), (2, 4), (3, 9), (4, 16)]

# square rooting second element of list element
square_root_second_element = list(map(lambda x: (x[0], math.sqrt(x[1])), values))

print(square_root_second_element)