"""
Written by: Irtiza Ali
Date: 17/09/2018
Desription: It contains an example related to python map functions
"""

values = [1, 2, 3, 4, 5]

# adding 10 to list
add_10 = list(map(lambda x: x + 10, values))

# condition statement for mapping
if_odd_10_if_even_20 = list(map(lambda x : x + 20  if x % 2 == 0 else x + 10, values))






print(add_10)
print(if_odd_10_if_even_20)
