"""
Written by: Irtiza Ali
Date: 20/09/2018
Desription: It contains an example related to python partial function. In this example we will print table of two using partials
"""

from functools import partial

def mul(a, b):
    return a * b

mul_2 = partial(mul, 2)
mul_3 = partial(mul, 3)


# printing table of 2
for num in range(1, 11):
    print("2 * " + str(num) + " = " + str(mul_2(num)))

print("\n")

# printing table of 3
for num in range(1, 11):
    print("2 * " + str(num) + " = " + str(mul_3(num)))
