# Partial Function

Sometimes we want to use the behavior of a function, but decrease the number of arguments it takes. The purpose is to "save" one of the inputs, and create a new function that defaults the behavior using the saved input params.