"""
Written by: Irtiza Ali
Date: 19/09/2018
Desription: It contains an example related to python filter function. In this example we will make even number
            odd and vice versa
"""
names = ['pakistan', 'america', 'india']

name_1 = list(filter(lambda x : 'pak' in x, names))

name_2 = list(filter(lambda x : 'ame' in x, names))

name_3 = list(filter(lambda x : 'ind' in x, names))

name_4 = list(filter(lambda x : 'a' in x, names))

print(name_1, name_2, name_3, name_4)