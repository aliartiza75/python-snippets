"""
Written by: Irtiza Ali
Date: 19/09/2018
Desription: It contains an example related to python filter function. It will differenciate 
            even and odd numbers
"""
nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

even = list(filter(lambda x : x  if x % 2 == 0 else None, nums))

odd = list(filter(lambda x: x if x % 2 != 0 else None, nums))

