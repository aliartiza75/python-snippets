"""
Written by: Irtiza Ali
Date: 4/09/2018
Desription: It contains tests and time taken to evalute those their results.
"""
import numpy as np
import numexpr as ne

a = np.arange(1e8)
b = np.arange(1e8)

a = (a ** 9) * 99 * (a ** 9)

"""
real	2m21.260s
user	0m14.437s
sys	  0m2.854s
"""

a = ne.evaluate("(a ** 9) * 99 * (a ** 9)") 

"""
real	0m37.897s
user	0m14.120s
sys	  0m2.020s
"""

