# numexpr module testing

test_numexpr.py file contains some tests and time taken to evalute their results. Calculated time given in the test_numexpr.py file might be differet on different systems based on the CPU usage.

Time was calculated using this command: 

    time python3 test_numexpr.py


Further details can be found on this link: https://github.com/pydata/numexpr
Link to gist: https://gist.github.com/aliartiza75/1cb3d1150d7939187e20778e91a03f69
