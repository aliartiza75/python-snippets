"""
Written by: Irtiza Ali
Date: 07/09/2018
Desription: It contains an example related to python dunder [magic methods, special method]
"""

class Car():

    def __init__(self):
        '''
        Constructor for the Car class
        '''
        self.model = None
        self.manufacturer = None
